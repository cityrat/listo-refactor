package com.listo;


import java.util.List;

import static com.listo.CharClass.*;
import static com.listo.Stats.abilityScoreImprovement;

public class Main {

    public static void main(String[] args) {
        Character c = new Character(null, null, null, null, 10);


        c.suggestedBackground = Background.getSuggestedBackground(c.charClass);
        System.out.println("suggested bg="+c.suggestedBackground);

//        Skills sk = new Skills();

        Character.setProf(c);
//        first decide class, go to background, then come back to class? this way the class skills added won't be the ones your background adds

        List<Integer> raw = Stats.getRawRolls();
        System.out.println("Initial rolls:  "+raw);
        Race.raceAdjustments(c);

        Background.selectBackground(c);
        Background.backgroundAdjustments(c);
//        Skills.addSkills(c, c.extraSkills); // this takes care of half elf TODO apply expertise of bard after this step
        CharClass.classAdjustments(c, raw);
        c.stats= Stats.assignStats(c.stats, c.preferredStats, raw);
        //stat bonuses from level (4th 8th level)
        abilityScoreImprovement(c);
        Spells.prepareForSpells(c);
        Spells.getSpells(c);
        // assign spells before weapons in case druid has shillelagh cantrip
        Weapons.assignWeapons(c);
        Armor.getArmor(c);
        c.abil.add("Panhandling");

        Languages.addLanguages(c);

        //adjust gold and magick items
////Adventurer's League chars have a trinket
//if (al==true)
//    eqOutput += ", Trinket";
        Character.calculateHitPoints(c);
        Stats.calculateSavingThrows(c);

//        System.out.println(c.skills.get(ACROBATICS.getSkillStat()));
//        System.out.println(c.skills.get(INTIMIDATION));
        c.ac = Armor.calculateAc(c);
        Character.printCharacter(c);
    }




}
