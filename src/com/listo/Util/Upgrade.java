package com.listo.Util;

import com.listo.Character;
import com.listo.Roll;

public class Upgrade {

    ////	lvl 2-30, 3-50, 4 70, 5 90 6+ automatic
    public static Boolean getsCommonUpgrade(Character c) {
        double x=0.3 + ((c.lvl -2) * .2);
        if (c.lvl ==1)
            x=0.0;
        if (Roll.d(100) * 0.01 < x)
            return true;
        else
            return false;

    }

    // 2-10, 3-17, 4-25, 6-40, 8-55, 10-70
    public static Boolean getsUncommonUpgrade(Character c) {
        double x=0.1 + ((c.lvl-2) * 0.075);
        if (c.lvl ==1)
            return false;
        if (Roll.d(100) * .01 < x)
            return true;
        else
            return false;
    }

    // 2-2, 4-8, 6-18, 8-32, 10-50
    public static Boolean getsRareUpgrade(Character c) {
        double x=((c.lvl * c.lvl) / 2) * .01;
        if (c.lvl ==1)
            return false;
        if (Roll.d(100) * .01 < x)
            return true;
        else
            return false;
    }

    public static Boolean getsVeryRareUpgrade(Character c) {
        double x=((c.lvl * c.lvl) / 4) * .01;
        if (c.lvl ==1)
            return false;
        if (Roll.d(100) * .01 < x)
            return true;
        else
            return false;
    }

//        function upgrade() {
////	lvl 2-30, 3-50, 4 70, 5 90 6+ automatic
//            x=0.3 + ((level-2) * .2);
//            if (level ==1)
//                x=0;
//            if (Math.random() < x)
//                return true;
//            else
//                return false;
//        }
//        function upgradeUncommon() {
//            // 2-10, 3-17, 4-25, 6-40, 8-55, 10-70
//            x=0.1 + ((level-2) * 0.075);
//            if (level ==1)
//                x=0;
//            if (Math.random() < x)
//                return true;
//            else
//                return false;
//        }
//        function upgradeRare() {
//            // 2-2, 4-8, 6-18, 8-32, 10-50
//            x=((level * level) / 2) * .01;
//            if (level ==1)
//                x=0;
//            if (Math.random() < x)
//                return true;
//            else
//                return false;
//        }
//        function upgradeVeryRare() {
//            // half as likely as upgraderare
//            x=((level * level) / 4) * .01;
//            if (level < 5)
//                x=0;
//            if (Math.random() < x)
//                return true;
//            else
//                return false;
//        }

}
