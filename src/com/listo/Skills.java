package com.listo;

import com.listo.Util.Util;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.listo.Stats.*;


//public enum Skills {
//    ACROBATICS(DEXTERITY, "Acrobatics", 0, 0),    // stat string know_it? final_value
//    ANIMAL_HANDLING(WISDOM, "Animal Handling", 0),
//    ARCANA(INTELLIGENCE, 0),
//    ATHLETICS(STRENGTH, 0),
//    DECEPTION(CHARISMA, 0),
//    HISTORY(INTELLIGENCE, 0),
//    INSIGHT(WISDOM, 0),
//    INTIMIDATION(CHARISMA, 0),
//    INVESTIGATION(INTELLIGENCE, 0),
//    MEDICINE(WISDOM, 0),
//    NATURE(INTELLIGENCE, 0),
//    PERCEPTION(WISDOM, 0),
//    PERFORMANCE(CHARISMA, 0),
//    PERSUASION(CHARISMA, 0),
//    RELIGION(INTELLIGENCE, 0),
//    SLEIGHT_OF_HAND(DEXTERITY, 0),
//    STEALTH(DEXTERITY, 0),
//    SURVIVAL(WISDOM, 0);
//
//    private final Stats skillStat;
//    private int value;
//    Skills(Stats skillStat, int value) {
//        this.skillStat = skillStat;
//        this.value = value;
//    }


public enum Skills implements Pickable {
    ACROBATICS("Acrobatics", DEXTERITY),
    ANIMAL_HANDLING("Animal Handling", WISDOM),
    ARCANA("Arcana",  INTELLIGENCE),
    ATHLETICS("Athletics", STRENGTH),
    DECEPTION("Deception", CHARISMA),
    HISTORY("History", INTELLIGENCE),
    INSIGHT("Insight", WISDOM),
    INTIMIDATION("Intimidation", CHARISMA),
    INVESTIGATION("Investigation", INTELLIGENCE),
    MEDICINE("Medicine", WISDOM),
    NATURE("Nature", INTELLIGENCE),
    PERCEPTION("Perception", WISDOM),
    PERFORMANCE("Performance", CHARISMA),
    PERSUASION("Persuasion", CHARISMA),
    RELIGION("Religion", INTELLIGENCE),
    SLEIGHT_OF_HAND("Sleight of Hand", DEXTERITY),
    STEALTH("Stealth", DEXTERITY),
    SURVIVAL("Survival", WISDOM);

    private final Stats skillStat;
    private final String name;
    Skills(String name, Stats skillStat) {
        this.name = name;
        this.skillStat = skillStat;
    }

    public static void addSkills(Character c, int choicesLeft, Skills[] choices) {
        while (choicesLeft > 0) {
            Skills randomSkill;
            randomSkill = (Skills)Util.pickOneEnum(choices);
//            int x = Roll.d(choices.size())-1;
//            Skills randomSkill = choices.get(x);

            //rogues like stealth
//            if (c.charClass==CharClass.ROGUE) {
//                int stealthRoll = Roll.d(6);
//                if (stealthRoll==6) {
//                    randomSkill=STEALTH;
//                }
//            }

            if (c.skills.get(randomSkill) == 0) {
               System.out.println(randomSkill + " added");
                c.skills.put(randomSkill, 1);
                choicesLeft--;
            } else {
                System.out.println("we already know " + randomSkill);
            }
        }
    }

    public Stats getSkillStat() {
        return skillStat;
    }

    public static String printOneSkill(Character c, Skills skill) {
        int x = c.skills.get(skill);
//        System.out.println(x);
        x = x * c.prof;
//        System.out.println(x);
        x += Util.abilityModifier(c.stats.get(skill.getSkillStat()));
//        System.out.println(Util.showPlus(x));
        return c.skills.get(skill)+" "+Util.showPlus(x);
    }

    public static String printSkills (Character c) {
        String result="";

        for (Skills skill : c.skills.keySet()) {
            result += skill.name + " " + printOneSkill(c, skill) +", ";
        }
        return result;
    }

    protected static void chooseRogueExpertises(Character c, int expertisesLeft) {
        while (expertisesLeft > 0) {
            List<Skills> choices = Arrays.asList(ACROBATICS,ANIMAL_HANDLING,ARCANA,ATHLETICS,DECEPTION,HISTORY,INSIGHT,INTIMIDATION,INVESTIGATION,MEDICINE,NATURE,PERCEPTION,PERFORMANCE,PERSUASION,RELIGION,SLEIGHT_OF_HAND,STEALTH,SURVIVAL);

            int roll = Roll.d(choices.size()) -1;
            Skills pick = choices.get(roll);

            //rogues like stealth
            if (c.charClass==CharClass.ROGUE) {
                int stealthRoll = Roll.d(10);
                if (stealthRoll==6) {
                    pick=STEALTH;
                }
            }

            if (c.skills.get(pick) == 1) {
                System.out.println("Made " + pick +" an expertise");
                c.skills.put(pick, 2);
                expertisesLeft--;
            } else {
                System.out.println("No change made on skill:  " + pick);
            }
        }
    }
}
