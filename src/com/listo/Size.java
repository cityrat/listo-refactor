package com.listo;

public enum Size {
    SMALL("Small"),
    MEDIUM("Medium");
    private final String name;
    Size (String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
