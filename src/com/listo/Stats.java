package com.listo;

import com.listo.Util.Util;

import java.util.*;

import static com.listo.CharClass.FIGHTER;

public enum Stats implements Pickable {

    STRENGTH, DEXTERITY, CONSTITUTION, INTELLIGENCE, WISDOM, CHARISMA;

    public static List<Integer> getRawRolls() {
//        int[] raw = {  };
        List<Integer> raw;
        int total;
        do {

            raw = new LinkedList<Integer>(Arrays.asList(Roll.best3of4(), Roll.best3of4(), Roll.best3of4(),
                    Roll.best3of4(), Roll.best3of4(), Roll.best3of4()));
            // order raw rolls starting with best roll and ending with worst
            Collections.sort(raw);
            Collections.reverse(raw);
            total=0;
            for (int i : raw) {
                total += i;
            }
//            System.out.println("total rolls="+total);
        } while (total < 65);

        return raw;

    }

    public static Integer assignIfEmpty(Integer stat, List<Integer> raw) {
        if (stat < 3) {
            stat += raw.remove(0);
            return stat;
        }
        return stat;
    }

    public static void calculateSavingThrows(Character c) {
        for (Stats stat : c.savingThrows.keySet()) {
            int prof=0;
            // add proficiency bonus if you are proficient with the save
            if (c.savingThrowProficiencies.get(stat)) {
                prof=c.prof;
            }
            // final result is ability modifier, plus proficiency if you're proficient
            c.savingThrows.put(stat, Util.abilityModifier(c.stats.get(stat)) + prof);
        }
    }

    public static Map<Stats, Integer> assignStats(Map<Stats, Integer> stats, List<Stats> preferredStats, List<Integer> raw) {
        for (Stats stat : preferredStats) {
            stats.put(stat, stats.get(stat)+raw.remove(0));
        }
        System.out.println(" class assigned these:  "+stats);

        // from now on every assignment will be randomly. order doesn't matter
        Collections.shuffle(raw);
        for (Stats stat : stats.keySet()) {
            stats.put(stat, assignIfEmpty(stats.get(stat), raw));
        }
        return stats;
    }


    public static boolean dexterityIsHigherThanStrength(Character c) {
        if (Util.abilityModifier(c.stats.get(DEXTERITY)) > Util.abilityModifier(c.stats.get(STRENGTH))) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean strengthIsHigherThanDexterity(Character c) {
        if (Util.abilityModifier(c.stats.get(STRENGTH)) > Util.abilityModifier(c.stats.get(DEXTERITY))) {
            return true;
        } else {
            return false;
        }

    }

    public static void abilityScoreImprovement(Character c) {
        int pointsLeft=0;
        if (c.lvl >= 4)
            pointsLeft+=2;
        if (c.lvl >= 8)
            pointsLeft+=2;
        if (c.lvl >= 12)
            pointsLeft+=2;
        if (c.lvl >= 16)
            pointsLeft+=2;
        if (c.lvl >= 19)
            pointsLeft+=2;
        if (c.charClass == FIGHTER) {
            if (c.lvl >= 6)
                pointsLeft+=2;
            if (c.lvl >= 14)
                pointsLeft+=2;
        }
        System.out.println("starting points for ability increase:" + pointsLeft);
//        HashMap<Stats, Integer> statIncreaseWeights = new HashMap<Stats, Integer>();
        Stats[] choices = new Stats[]{ STRENGTH, DEXTERITY, CONSTITUTION, WISDOM, INTELLIGENCE, CHARISMA};
        int[] weights = new int[] { 0,0,0,0,0,0};
        HashMap<Stats, Integer>  increaseHistory = new HashMap<Stats, Integer>();
        for (Stats s : Stats.values()) {
            increaseHistory.put(s,0);
        }
        while (pointsLeft > 0) {
            for (int i=0; i < 6; i ++) {
                int weight = assesStatForIncrease(c, choices[i]);
                weights[i] = weight;
            }
            Stats statChoice = (Stats) Util.pickOneEnumWeighted( choices, weights);
            System.out.println("choice made=" + statChoice);
            increaseHistory.put(statChoice, increaseHistory.get(statChoice)+1);
            c.stats.put(statChoice, c.stats.get(statChoice) + 1);
            pointsLeft--;
        }
        System.out.print("Increase history  == "+ increaseHistory);
    }

    private static int assesStatForIncrease(Character c, Stats stat) {
        if (c.stats.get(stat) == 20) {
            return 0;
        }
        int likely=0;
        for (int i=0; i < c.preferredStats.size(); i++) {
//            System.out.println(i + " is " + c.preferredStats.get(i));
            if (c.preferredStats.get(i).equals(stat)) {
                if (i==0) {
                    likely=60;
                } else if (i==1) {
                    likely=50;
                } else if (i==2) {
                    likely=40;
                } else {
                    likely=30;
                }
            } else {
                likely=10;
            }
        }
        if (c.stats.get(stat) % 2 == 1) {
            likely += 20;
        }
        return likely;
    }
}


