package com.listo;

public enum DamageType {
    SLASHING ("slashing"),
    PIERCING ("piercing"),
    BLUDGEONING ("bludgeoning");

    private final String name;
    DamageType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
