package com.listo;

import com.listo.Util.Util;

import static com.listo.CharClass.*;
import static com.listo.Stats.*;

public enum Armor implements Pickable {
    NONE("None",10),
    PADDED("Padded",11),
    LEATHER("Leather",11),
    STUDDED_LEATHER("Studded leather",12),
    //medium
    HIDE("Hide",12),
    CHAIN_SHIRT("Chain shirt",13),
    SCALE_MAIL("Scale mail",14),
    BREASTPLATE("Breastplate",14),
    HALF_PLATE("Half plate",15),
    //heavy
    RING_MAIL("Ring mail",14),
    CHAIN_MAIL("Chain mail",16),
    SPLINT_MAIL("Splint mail",17),
    PLATE_MAIL("Plate mail",18);

    private final String name;
    private final int baseAc;

    Armor(String name, int baseAc) {
        this.name = name;
        this.baseAc = baseAc;
    }

    public static void assignArmor(Character c) {
    }


    public String getName() {
        return name;
    }

    public int getBaseAc() {
        return baseAc;
    }


//0 PADDED(
//1 LEATHER(
//2  STUDDED LEATHER(
//3 HIDE(
//4 CHAIN SHIRT(
//5 SCALE MAIL(
//6 BREASTPLATE(
//7 HALF PLATE(
//8 RING MAIL(
//9 CHAIN MAIL(
//0 SPLINT MAIL(
//11PLATE MAIL(



    public static int calculateAc (Character c) {

        int shieldBonus=0;
        switch (c.charClass) {
            case BARBARIAN:
                if (c.hasShield)
                    shieldBonus=2;
                return 10 + Util.abilityModifier(c.stats.get(DEXTERITY)) + Util.abilityModifier(c.stats.get(CONSTITUTION)) + shieldBonus;
            case MONK:
                return 10 + Util.abilityModifier(c.stats.get(DEXTERITY)) + Util.abilityModifier(c.stats.get(WISDOM));
// dragon sorcerers have special ac
//            case SORCERER:
// TODO dragpn sorcerer

            default:
                if (c.hasShield)
                    shieldBonus=2;
                int dexBonus = Util.abilityModifier(c.stats.get(DEXTERITY));
                return c.armorWorn.getBaseAc() + dexBonus + shieldBonus;
        }
    }

    public static void getArmor (Character  c) {
        switch (c.charClass) {
            case BARBARIAN:
                break;
            case BARD:
                boolean canMedium=false;
                if (c.archetype.contains("Valor")) {
                    canMedium=true;
                }
                assignArmor(c, true, canMedium, false);
                break;
            case CLERIC:
                boolean canHeavy =false;
                if (c.archetype.contains("Life") || c.archetype.contains("Nature") || c.archetype.contains("Tempest") ||
                        c.archetype.contains("War")) {
                    canHeavy=true;
                }
                assignArmor(c, true, true, canHeavy);
                break;
            case DRUID:
                break;
            case FIGHTER:
                assignArmor(c, true, true, true);
                break;
            case MONK:
                // nothing to do here
                break;
            case PALADIN:
                assignArmor(c, true, true, true);
                break;
            case RANGER:
                assignArmor(c, true, true, false);
                break;
            case ROGUE:
                assignArmor(c, true, false, false);
                break;
            case SORCERER:
                assignArmor(c, false, false, false);
                break;
            case WARLOCK:
                break;
            case WIZARD:
                assignArmor(c, false, false, false);
                break;
        }
    }

    public static void assignArmor (Character c, boolean canWearLightArmor, boolean canWearMediumArmor, boolean canWearHeavyArmor) {
        int dexBonus = Util.abilityModifier(c.stats.get(DEXTERITY));
        int strength=c.stats.get(STRENGTH);
        if (canWearHeavyArmor && strength >= 15) {
            c.armorWorn=PLATE_MAIL;
        } else if (canWearHeavyArmor && strength >= 13) {
            c.armorWorn=CHAIN_MAIL;
        } else if (canWearMediumArmor) {
            c.armorWorn=CHAIN_SHIRT;
        } else if (canWearLightArmor) {
            c.armorWorn=LEATHER;
        } else {
            c.armorWorn=NONE;
        }
    }
}
