package com.listo;

public enum FightingStyle {
    ARCHERY("Archery"),
    DEFENSE("Defense"),
    DUELING("Dueling"),
    GREAT_WEAPON_FIGHTING("Great weapon fighting"),
    PROTECTION("Protection"),
    TWO_WEAPON_FIGHTING("Two-weapon fighting");

    private final String name;
    FightingStyle (String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
