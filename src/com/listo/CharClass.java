package com.listo;

import com.listo.Util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.listo.Stats.*;

public enum CharClass implements Pickable {
    BARBARIAN("Barbarian"),
    BARD("Bard"),
    CLERIC("Cleric"),
    DRUID("Druid"),
    FIGHTER("Fighter"),
    MONK("Monk"),
    PALADIN("Paladin"),
    RANGER("Ranger"),
    ROGUE("Rogue"),
    SORCERER("Sorcerer"),
    WARLOCK("Warlock"),
    WIZARD("Wizard");

    private final String name;
    CharClass (String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public static CharClass selectClass() {
        CharClass[] classes= {BARBARIAN,BARD,CLERIC,DRUID,FIGHTER,FIGHTER,FIGHTER,MONK,PALADIN,RANGER,ROGUE,SORCERER,WARLOCK,WIZARD};
        int roll = Roll.d(classes.length)-1;
        final CharClass c= classes[roll];
        System.out.println("++++++++++++++++++++++++"+c);
        return c;
    }


    public static void classAdjustments(Character c, List<Integer> raw) {
        switch (c.charClass) {
            case BARBARIAN:
                c.preferredStats = new ArrayList<>(Arrays.asList(STRENGTH, CONSTITUTION, DEXTERITY));
                c.savingThrowProficiencies.put(STRENGTH, true);
                c.savingThrowProficiencies.put(CONSTITUTION, true);
                c.hd = 12;
                CharClassDetails.barbarian(c);
                break;
            case BARD:
                c.preferredStats = new ArrayList<>(Arrays.asList(CHARISMA, DEXTERITY));
                c.savingThrowProficiencies.put(CHARISMA, true);
                c.savingThrowProficiencies.put(DEXTERITY, true);
                c.spellAbility=CHARISMA;
                c.hd = 8;
                CharClassDetails.bard(c);
                break;
            case CLERIC:
                c.preferredStats = new ArrayList<>(Arrays.asList(WISDOM, STRENGTH, CONSTITUTION));
                c.savingThrowProficiencies.put(WISDOM, true);
                c.savingThrowProficiencies.put(CHARISMA, true);
                c.spellAbility=WISDOM;
                c.hd = 8;
                CharClassDetails.cleric(c);
                break;
            case DRUID:
                c.preferredStats = new ArrayList<>(Arrays.asList(WISDOM, CONSTITUTION));
                c.savingThrowProficiencies.put(WISDOM, true);
                c.savingThrowProficiencies.put(INTELLIGENCE, true);

                c.spellAbility=WISDOM;
                c.hd = 8;
                CharClassDetails.druid(c);
                break;
            case FIGHTER:
                int roll=Roll.d(3);
                //Eldritch Knight
                if (roll == 1) {
                    c.preferredStats = new ArrayList<>(Arrays.asList(STRENGTH, INTELLIGENCE, CONSTITUTION, DEXTERITY));
                    c.spellAbility=INTELLIGENCE;
                    if (c.lvl >= 3) c.archetype="Eldritch Knight";
                // high dex
                } else if (roll == 2) {
                    c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, CONSTITUTION, STRENGTH));
                // high str
                } else {
                    c.preferredStats = new ArrayList<>(Arrays.asList(STRENGTH, CONSTITUTION, DEXTERITY));
                }
                c.savingThrowProficiencies.put(STRENGTH, true);
                c.savingThrowProficiencies.put(CONSTITUTION, true);
                if ((c.lvl >= 3) && roll >= 2) {
                    int roll2 = Roll.d(2);
                    if (roll2 == 1) {
                        c.archetype="Champion";
                    } else {
                        c.archetype="Battlemaster";
                    }
                }
                c.hd = 10;
                CharClassDetails.fighter(c);
                break;
            case MONK:
                c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, WISDOM, CONSTITUTION));
                c.savingThrowProficiencies.put(STRENGTH, true);
                c.savingThrowProficiencies.put(DEXTERITY, true);
                c.spellAbility=WISDOM;
                c.hd = 8;
                CharClassDetails.monk(c);
                break;
            case PALADIN:
                c.preferredStats = new ArrayList<>(Arrays.asList(STRENGTH, CHARISMA, CONSTITUTION, WISDOM));
                c.savingThrowProficiencies.put(WISDOM, true);
                c.savingThrowProficiencies.put(CHARISMA, true);

                c.spellAbility=WISDOM;
                c.hd = 10;
                CharClassDetails.paladin(c);
                break;
            case RANGER:
                c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, WISDOM, CONSTITUTION));
                c.savingThrowProficiencies.put(STRENGTH, true);
                c.savingThrowProficiencies.put(DEXTERITY, true);
                c.spellAbility=WISDOM;
                c.hd = 10;
                CharClassDetails.ranger(c);
                break;
            case ROGUE:
                roll=Roll.d(3);
                //arcane trickster
                if (roll == 1) {
                c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, INTELLIGENCE, CONSTITUTION));
                c.spellAbility=INTELLIGENCE;
                if (c.lvl >= 3) c.archetype="Arcane Trickster";
                    // thief
                } else if (roll == 2) {
                    if (c.lvl >= 3) c.archetype="Thief";
                    // assassin
                } else {
                    if (c.lvl >= 3) c.archetype="Assassin";
                }
                if (roll >= 2) {
                    if (Roll.d(2) == 2) {
                        c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, CONSTITUTION, CHARISMA));

                    } else {
                        c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, CONSTITUTION, INTELLIGENCE));
                    }
                }

                c.savingThrowProficiencies.put(DEXTERITY, true);
                c.savingThrowProficiencies.put(INTELLIGENCE, true);
                c.hd = 8;
                CharClassDetails.rogue(c);
                break;
            case SORCERER:
                c.preferredStats = new ArrayList<>(Arrays.asList(CHARISMA, CONSTITUTION));
                c.savingThrowProficiencies.put(CHARISMA, true);
                c.savingThrowProficiencies.put(CONSTITUTION, true);
                c.spellAbility=CHARISMA;
                c.hd = 6;
                CharClassDetails.sorcerer(c);
                break;
            case WARLOCK:
                if (Roll.d(2)==2) {
                    c.preferredStats = new ArrayList<>(Arrays.asList(CHARISMA, DEXTERITY, CONSTITUTION));
                } else {
                    c.preferredStats = new ArrayList<>(Arrays.asList(CHARISMA, STRENGTH, CONSTITUTION));
                }
                c.savingThrowProficiencies.put(WISDOM, true);
                c.savingThrowProficiencies.put(CHARISMA, true);
                c.spellAbility=CHARISMA;
                c.hd = 8;
                CharClassDetails.warlock(c);
                break;
            case WIZARD:
                c.preferredStats = new ArrayList<>(Arrays.asList(INTELLIGENCE, DEXTERITY, CONSTITUTION));
                c.savingThrowProficiencies.put(INTELLIGENCE, true);
                c.savingThrowProficiencies.put(WISDOM, true);
                c.spellAbility=INTELLIGENCE;;
                c.hd = 6;
                CharClassDetails.wizard(c);
                break;
            default:
                System.out.println("error no class");
                break;
          }
//
        System.out.println(raw);

    }


}
