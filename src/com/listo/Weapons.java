package com.listo;

import com.listo.Util.Upgrade;
import com.listo.Util.Util;


import static com.listo.CharClass.MONK;
import static com.listo.DamageType.*;
import static com.listo.Race.*;
import static com.listo.Size.SMALL;
import static com.listo.Stats.*;
import static com.listo.Util.Upgrade.getsCommonUpgrade;
import static com.listo.Util.Upgrade.getsUncommonUpgrade;

public enum Weapons implements Pickable {
    DAGGER("Dagger", "1d4", PIERCING, true, true, "20/60", null),
    MACE("Mace", "1d6", BLUDGEONING, false, false , null, null),
    SPEAR("Spear", "1d6(1d8)", PIERCING, false, true, "20/60", null),
    HANDAXE("Handaxe", "1d6", SLASHING, false, true, "20/60", null),
    JAVELIN("Javelin","1d6",PIERCING, false, true, "30/120", null),
    LIGHT_CROSSBOW("Light crossbow","1d8",PIERCING, true, false, "80/320", "bolts"),
    HEAVY_CROSSBOW("Heavy crossbow","1d10",PIERCING, true, false, "100/400", "bolts"),
    HAND_CROSSBOW("Hand crossbow","1d6",PIERCING, true, false, "30/120", "bolts"),
    SHORT_BOW("Short bow","1d6",PIERCING, true, false, "80/320", "arrows"),
    LONG_BOW("Long bow","1d8",PIERCING, true, false, "150/600", "arrows"),
    THROWING_HAMMER("Throwing hammer","1d4",BLUDGEONING, false, true, "20/60", null),
    BATTLEAXE("Battleaxe","1d8(1d10)",SLASHING, false, false, null, null),
    GREATSWORD("Greatsword","2d6",SLASHING, false, false, null, null),
    HALBERD("Halberd","1d10",SLASHING, false, false, null, null),
    LONGSWORD("Longsword","1d8(1d10)",SLASHING, false, false, null, null),
    MORNING_STAR("Morning Star","1d8",PIERCING, false, false, null, null),
    RAPIER("Rapier","1d8",PIERCING, true, false, null, null),
    SCIMITAR("Scimitar","1d6",SLASHING, true, false, null, null),
    SHORTSWORD("Shortsword","1d6",PIERCING, true, false, null, null),
    WARHAMMER("Warhammer","1d8(1d10)",BLUDGEONING, false, false, null, null),
    GREAT_AXE("Great Axe","1d12",SLASHING, false, false, null, null),
    DART("Dart","1d4",PIERCING, true, true, "20/60", null),
    QUARTERSTAFF("Quarterstaff","1d6(1d8)",BLUDGEONING, false, false, null, null),
    UNARMED_STRIKE( "Unarmed Strike","1d4",BLUDGEONING, true, false, null, null),
    SLING("Sling","1d4",BLUDGEONING, true, false,"30/120", "bullets");

    private final String name;
    private final String damage;
    private final DamageType damageType;
    private final Boolean getsDexBonus;
    private final Boolean isThrowable;
    private final String range;
    private final String ammoName;

    Weapons(String name, String damage, DamageType damageType, Boolean getsDexBonus, Boolean isThrowable, String range, String ammoName) {
        this.name = name;
        this.damage = damage;
        this.damageType = damageType;
        this.getsDexBonus = getsDexBonus;
        this.isThrowable = isThrowable;
        this.range = range;
        this.ammoName = ammoName;
    }
    public static int calcToHit(Character c, Weapons weapon) {
        int total = c.prof;
        if (weapon.getFinesse()) {
            total += Util.abilityModifier(c.stats.get(DEXTERITY));
        } else {
            total += Util.abilityModifier(c.stats.get(STRENGTH));
        }
        return total;
    }

    public static int calcDamageBonus(Character c, Weapons weapon) {
        if (weapon.getFinesse()) {
            return Util.abilityModifier(c.stats.get(DEXTERITY));
        } else {
            return Util.abilityModifier(c.stats.get(STRENGTH));
        }
    }

    //has special logic due to damage of unamred strike changing by level
    public static String showMeleeWeapon(Character c) {
        if (c.meleeWeapon != null) {
            if (c.charClass==MONK && c.meleeWeapon==UNARMED_STRIKE) {

                String monkDamage="d4";
                if (c.lvl >= 5) {
                    monkDamage="d6";
                }

                if (c.lvl >= 11) {
                    monkDamage="d8";
                }

                if (c.lvl >= 17) {
                    monkDamage="d10";
                }

                return(c.meleeWeapon.getName() + " (" +
                        Util.showPlus(Weapons.calcToHit(c, c.meleeWeapon)) + " to hit) " +
                        monkDamage +
                        Util.showPlusNotZero(Weapons.calcDamageBonus(c, c.meleeWeapon)) + " " +
                        c.meleeWeapon.getDamageType().getName());

            } else {
                return(c.meleeWeapon.getName() + " (" +
                        Util.showPlus(Weapons.calcToHit(c, c.meleeWeapon)) + " to hit) " +
                        c.meleeWeapon.getDamage() +
                        Util.showPlusNotZero(Weapons.calcDamageBonus(c, c.meleeWeapon)) + " " +
                        c.meleeWeapon.getDamageType().getName());

            }
        }
        return "";
    }

    public String getName() {
        return name;
    }

    public String getDamage() {
        return damage;
    }

    public DamageType getDamageType() {
        return damageType;
    }

    public Boolean getFinesse() {
        return getsDexBonus;
    }

    public Boolean getThrowable() {
        return isThrowable;
    }

    public String getRange() {
        return range;
    }

    public String getAmmoName() {
        return ammoName;
    }



    public static void assignWeapons(Character c) {
        Weapons[] choices;
        int[] weights;

        if (c.race==DROW_ELF && getsUncommonUpgrade(c)) {
            c.handCrossbowWeapon = HAND_CROSSBOW;
        }

        switch(c.charClass) {
            case BARBARIAN:
                if (c.size == SMALL) {
                    c.meleeWeapon = (Weapons)Util.pickOneEnum(new Weapons[]{BATTLEAXE, LONGSWORD, WARHAMMER});
                } else {
                    choices = new Weapons[]{GREAT_AXE, BATTLEAXE, GREATSWORD, WARHAMMER};
                    weights = new int[]{7,3,4,3};
                    c.meleeWeapon = (Weapons)Util.pickOneEnumWeighted(choices, weights);
                }
                if (c.lvl > 1) {
                    if (Roll.d(4) == 4) {
                        c.thrownWeapon = HANDAXE;
                        c.thrownWeaponNumber = Roll.d(3) + 2;
                    } else {
                        do {
                            c.backupWeapon= (Weapons) Util.pickOneEnum(
                                    new Weapons[]{BATTLEAXE, LONGSWORD, WARHAMMER, MORNING_STAR});
                        } while (c.meleeWeapon == c.backupWeapon);
                    }
                }
                // give a bow to those with decent dex score
                if ((c.lvl > 1) && Util.abilityModifier(c.stats.get(DEXTERITY)) >= 0) {
                    if (c.size == SMALL) {
                        c.rangedWeapon = SHORT_BOW;
                    } else {
                        c.rangedWeapon = LONG_BOW;
                    }
                } else {
                    c.rangedWeapon = JAVELIN;
                    c.rangedWeaponNumber= Roll.d(5) +3;
                }
                break;
            case BARD:
                if (Util.abilityModifier(c.stats.get(DEXTERITY)) > Util.abilityModifier(c.stats.get(STRENGTH))) {
                    c.meleeWeapon = RAPIER;
                } else {
                    c.meleeWeapon = LONGSWORD;
                }
                if (!elfBowUpgrade(c)) {
                    if (c.lvl > 1 && (c.archetype=="College of Valor") && (c.size != SMALL)) {
                        c.rangedWeapon = (Weapons) Util.pickOneEnum(new Weapons[]{LONG_BOW, HEAVY_CROSSBOW});
                    } else {
                        c.rangedWeapon = (Weapons) Util.pickOneEnum(new Weapons[]{SHORT_BOW, LIGHT_CROSSBOW});
                    }
                }
                c.thrownWeapon=DAGGER;
                if (getsCommonUpgrade(c)) {
                    c.thrownWeaponNumber = Roll.d(3) + 2;
                } else {
                    c.thrownWeaponNumber = 1;
                }
                if (getsUncommonUpgrade(c)) {
                    c.handCrossbowWeapon = HAND_CROSSBOW;
                }
                break;
            case CLERIC:
                boolean martialWeaponProficiency = false;
                if (c.archetype.contains("War") || c.archetype.contains("Tempest")) {
                    martialWeaponProficiency=true;
                }
                if (!elfMeleeUpgrade(c)) {
                    if (!dwarfMeleeUpgrade(c)) {
                        if (martialWeaponProficiency) {
                            c.meleeWeapon = (Weapons)Util.pickOneEnum(new Weapons[]{BATTLEAXE, LONGSWORD, MORNING_STAR, WARHAMMER});
                        } else {
                            choices = new Weapons[]{MACE, SPEAR, QUARTERSTAFF, HANDAXE};
                            weights = new int[]{5,2,3,2};
                            c.meleeWeapon = (Weapons)Util.pickOneEnumWeighted(choices, weights);
                        }
                    }
                }
                c.thrownWeapon = (Weapons)Util.pickOneEnum(new Weapons[] {SPEAR, HANDAXE});
                c.thrownWeaponNumber=1;
                if (!elfBowUpgrade(c)) {
                    if (martialWeaponProficiency) {
                        c.rangedWeapon = (Weapons)Util.pickOneEnum(new Weapons[]{HEAVY_CROSSBOW, LONG_BOW});
                    } else {
                        c.rangedWeapon = LIGHT_CROSSBOW;
                    }
                }

                break;
            case DRUID:

                // refactor to check for shillelagh cantrip

                if (!elfMeleeUpgrade(c)) {
                    if (!dwarfMeleeUpgrade(c)) {
                        if (strengthIsHigherThanDexterity(c)) {
                            choices = new Weapons[]{MACE, SPEAR, QUARTERSTAFF};
                            c.meleeWeapon = (Weapons)Util.pickOneEnum(choices);
                        } else {
                            c.meleeWeapon = SCIMITAR;
                        }
                    }
                }
                if (!elfBowUpgrade(c)) {
                    if (c.lvl ==1) {
                        choices = new Weapons[]{SLING, DART, JAVELIN};
                        weights = new int[]{2, 1, 2};
                    } else {
                        choices = new Weapons[]{SLING, JAVELIN};
                        weights = new int[]{3, 4};
                    }
                    Weapons choice = (Weapons)Util.pickOneEnumWeighted(choices, weights);
                    if (choice==SLING) {
                        c.rangedWeapon=SLING;
                    } else if (choice==DART) {
                        c.thrownWeapon=DART;
                        c.thrownWeaponNumber=10;
                    } else {
                        c.thrownWeapon=JAVELIN;
                        c.thrownWeaponNumber=Roll.d(3) +2;
                    }

                }
                break;
            case FIGHTER:
//                chooseMartialWeapon(c);
                break;
            case MONK:
                c.meleeWeapon=UNARMED_STRIKE;
                choices = new Weapons[]{SHORTSWORD, QUARTERSTAFF, SPEAR};
                weights = new int[]{5, 3, 3};
                c.backupWeapon = (Weapons)Util.pickOneEnumWeighted(choices, weights);

                if (!elfBowUpgrade(c)) {
                    if (c.lvl == 1) {
                        c.thrownWeapon=DART;
                        c.thrownWeaponNumber=10;
                    } else {
                        c.rangedWeapon = (Weapons)Util.pickOneEnum(new Weapons[]{SHORT_BOW, LIGHT_CROSSBOW});
                    }
                }

                break;
            case PALADIN:
                                chooseMartialWeapon(c);
                break;
            case RANGER:
                //                chooseMartialWeapon(c);
                break;
            case ROGUE:
                if (Roll.d(2) == 2) {
                    c.meleeWeapon=RAPIER;
                } else {
                    c.meleeWeapon=SHORTSWORD;
                }
                if (!elfBowUpgrade(c)) {
                    c.rangedWeapon=SHORT_BOW;
                }
                if (getsUncommonUpgrade(c)) {
                    c.handCrossbowWeapon=HAND_CROSSBOW;
                }
                c.thrownWeapon=DAGGER;
                if (getsCommonUpgrade(c)) {
                    c.thrownWeaponNumber=Roll.d(3)+2;
                } else {
                    c.thrownWeaponNumber=2;
                }
                break;
            case SORCERER:
                chooseMagicUserWeapons(c);
                break;
            case WARLOCK:
                if (!elfMeleeUpgrade(c)) {
                    if (!dwarfMeleeUpgrade(c)) {
                        choices = new Weapons[]{MACE, SPEAR, QUARTERSTAFF, HANDAXE};
                        weights = new int[]{2,6,2,2};
                        c.meleeWeapon = (Weapons)Util.pickOneEnumWeighted(choices, weights);
                    }
                }
                c.thrownWeapon=DAGGER;
                if (getsCommonUpgrade(c)) {
                    c.thrownWeaponNumber=Roll.d(3)+2;
                } else {
                    c.thrownWeaponNumber=2;
                }
                if (!elfBowUpgrade(c)) {
                    c.rangedWeapon=(Weapons)Util.pickOneEnum(new Weapons[]{SHORT_BOW, LIGHT_CROSSBOW});
                }
                break;
            case WIZARD:
                chooseMagicUserWeapons(c);
                break;
        }
    }

    private static boolean chooseMartialWeapon(Character c) {
        return false;
//        case 5:
//        warriorArmor();
//        z=Math.floor((Math.random() * 2));
//        // 0=great weapon, 1=single hand weapon, 2=2 weapons
//        if (fiStyle ==3)
//            z=0;
//        if ((fiStyle ==2) || (fiStyle ==4))
//            z=1;
//        if (fiStyle ==5)
//            z=2;
//        console.log("weapon choice:  " + z + " / fighting style: " + fiStyle);
//        if ((z==0) && (size != "Small")) {
//            x = pickWeap([13,20,12]);
//            myWp.push(weapons[x]);
//        }
//        if ((z==0) && (size == "Small")) {
//            x = pickWeap([11,14,19]);
//            myWp.push(weapons[x]);
//        }
//        if (z==1) {
//            myAr.push(armors[0]);
//            //rapier for high dex, otherwise random 1 handed weapon
//            if (mod[1] < mod[0]) {
//                x = pickWeap([11,14,15,19]);
//                myWp.push(weapons[x]);
//            }
//            else
//                myWp.push(weapons[16]);
//        }
//        if (z==2) {
//            x = pickWeap([17,18]);
//            myWp.push(weapons[x]);
//            wpNum[x] = 2;
//        }
//        //backup weapon
//        if (level>1) {
//            x = pickWeap([14,2,11]);
//            while (myWp.indexOf(weapons[x]) != -1) {
//                x = pickWeap([14,2,11]);
//            }
//            myWp.push(weapons[x]);
//        }
//
//
//        x=Math.random();
//        if (x < 0.6) {
//            if (x > 0.3) {
//                myWp.push(weapons[3]);
//                wpNum[3] = 3;
//            } else {
//                myWp.push(weapons[4]);
//                wpNum[4] = 3;
//                if (level > 1) {
//                    wpNum[4] = Math.floor(Math.random() * 3) +3;
//                }
//            }
//        }
//        if (size == "Small") {
//            if (Math.random() > 0.33) {
//                myWp.push(weapons[8]);
//            } else {
//                myWp.push(weapons[5]);
//            }
//        } else {
//            if (Math.random() > 0.33) {
//                myWp.push(weapons[9]);
//            } else  {
//                myWp.push(weapons[6]);
//            }
//        }

    }

    private static void chooseMagicUserWeapons(Character c) {
        if (!elfMeleeUpgrade(c)) {
            if (!dwarfMeleeUpgrade(c)) {
                if (strengthIsHigherThanDexterity(c)) {
                    c.meleeWeapon=QUARTERSTAFF;
                } else {
                    c.thrownWeapon=DAGGER;
                    c.thrownWeaponNumber=1;
                }
                if (getsCommonUpgrade(c)) {
                    c.thrownWeaponNumber=Roll.d(3) +2;
                }
                if (getsUncommonUpgrade(c)) {
                    c.thrownWeaponNumber=Roll.d(3) +4;
                }

            }
        }
        if (!elfBowUpgrade(c)) {
            Weapons[] choices;
            int[] weights;
            choices = new Weapons[]{LIGHT_CROSSBOW, DART, DAGGER};
            weights = new int[]{7,3,2};
            Weapons choice = (Weapons)Util.pickOneEnumWeighted(choices, weights);
            // can overwrite dagger weapon set above in melee weapon upgrade
            switch (choice) {
                case DAGGER:
                    c.thrownWeapon=DAGGER;
                    c.thrownWeaponNumber=Roll.d(3)+4;
                    break;
                case DART:
                    c.thrownWeapon=DART;
                    c.thrownWeaponNumber=10;
                    break;
                case LIGHT_CROSSBOW:
                    c.rangedWeapon=LIGHT_CROSSBOW;
                    break;
            }
        }


//            x = Math.random();
//            if (x > 0.75) {
//                wpNum[0] = Math.floor(Math.random() * 3) + 4;
//            } else if (x > 0.25) {
//                myWp.push(weapons[5]);
//            } else {
//                myWp.push(weapons[21]);
//                wpNum[21] = Math.floor(Math.random() * 3) + 9;
//            }
//
//        }
    }

    // what about drow hand crossbow?
    private static boolean elfBowUpgrade(Character c) {
        if (c.race == WOOD_ELF || c.race== HIGH_ELF) {

                c.rangedWeapon=LONG_BOW;
                return true;

        }
        return false;

    }

    private static boolean elfMeleeUpgrade(Character c) {
        if ((c.race == WOOD_ELF || c.race== HIGH_ELF)) {

            if (dexterityIsHigherThanStrength(c)) {
                c.meleeWeapon = SHORTSWORD;
                return true;
            } else {
                c.meleeWeapon = LONGSWORD;
                return true;
            }
        }
        if (c.race == DROW_ELF) {
            if (dexterityIsHigherThanStrength(c)) {
                c.meleeWeapon = RAPIER;
                return true;
            }
        }
        return false;
    }

    private static boolean dwarfMeleeUpgrade(Character c) {
        if ((c.race == HILL_DWARF || c.race== MOUNTAIN_DWARF)) {
            c.meleeWeapon = (Weapons)Util.pickOneEnum(new Weapons[]{BATTLEAXE, WARHAMMER});
            return true;
        }
        return false;
    }

//    VM174:1 0Dagger
//    VM174:1 1Mace
//    VM174:1 2Spear
//    VM174:1 3Handaxe
//    VM174:1 4Javelin
//    VM174:1 5Light crossbow
//    VM174:1 6Heavy crossbow
//    VM174:1 7Hand crossbow
//    VM174:1 8Short bow
//    VM174:1 9Long bow
//    VM174:1 10Throwing hammer
//    VM174:1 11Battleaxe
//    VM174:1 12Greatsword
//    VM174:1 13Halberd
//    VM174:1 14Longsword
//    VM174:1 15Morning Star
//    VM174:1 16Rapier
//    VM174:1 17Scimitar
//    VM174:1 18Shortsword
//    VM174:1 19Warhammer
//    VM174:1 20Great Axe
//    VM174:1 21Dart
//    VM174:1 22Quarterstaff
//    VM174:1 23Unarmed Strike
//    VM174:1 24Sling

}
