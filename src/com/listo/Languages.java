package com.listo;

import com.listo.Util.Util;

import java.util.List;

import static com.listo.Background.*;

public enum Languages implements Pickable {
//extra language  auran

    AURAN("Auran"), AARAKOCRA("Aarakocra"), THIEVES_CANT("Thieves' Cant"), DRUIDIC("Druidic"),
    COMMON("Common"),  ABYSSAL("Abyssal"), CELESTIAL("Celestial"), DRACONIC("Draconic"),
    DEEP_SPEECH("Deep Speech"), INFERNAL("Infernal"), PRIMORDIAL("Primordial"),
    SYLVAN("Sylvan"), UNDERCOMMON("Undercommon"), DWARVISH("Dwarvish"),
    ELVISH("Elvish"), GIANT("Giant"), GOBLIN("Goblin"), GNOMISH("Gnomish"),
    HALFLING("Halfling"), ORC ("Orc");

    private final String name;
    Languages (String name) {
        this.name = name;
    }


    public static void addLanguages(Character c) {
        Languages[] choices;
//        Languages[] exotics = new Languages[]{AURAN,  ABYSSAL, CELESTIAL, DRACONIC,
//                DEEP_SPEECH, INFERNAL, PRIMORDIAL, SYLVAN, UNDERCOMMON};
//        Languages[] mundanes=new Languages[]{DWARVISH, ELVISH, GIANT, GOBLIN, GNOMISH, HALFLING, ORC};
        Languages pick;
        while (c.extraLanguages > 0) {
            // 1 in 9 chance of knowing exotic language instead of normal one. 1 in 3 for sages
            // TODO knowledge clerics more exotics?
            // TODO acolytes more exotics?
            int x=Roll.d(9);
            if (c.background == SAGE) {
                x+=2;
            }
            if (x >= 9) {
                choices = new Languages[]{AURAN,  ABYSSAL, CELESTIAL, DRACONIC,
                        DEEP_SPEECH, INFERNAL, PRIMORDIAL, SYLVAN, UNDERCOMMON};
            } else {
                choices=new Languages[]{DWARVISH, ELVISH, GIANT, GOBLIN, GNOMISH, HALFLING, ORC};
            }
            int roll=Roll.d(choices.length) -1;
            pick=choices[roll];
            // only add it if we don't know it
            if (!c.languages.contains(pick)) {
                System.out.println(pick + " added");
                c.languages.add(pick);
                c.extraLanguages--;
            } else {
                System.out.println("We already know " + pick);
            }
        }
    }

    public static String displayLanguages(List<Languages> languages) {
        String result="";
        for (Languages l : languages) {
            result += l.name + ", ";
        }
        result=result.substring(0, result.length()-2);
        return result;
    }
}
