package com.listo;

import com.listo.Util.Const;
import com.listo.Util.Util;

import java.util.*;

import static com.listo.Gender.*;
import static com.listo.Languages.*;
import static com.listo.Size.*;
import static com.listo.Stats.*;

public class Character {

    public String name;
    public Integer lvl=0;
    protected Map<Stats, Integer> stats;
    protected Map<Stats, Boolean> savingThrowProficiencies;
    protected Map<Stats, Integer> savingThrows;
    protected Map<Skills, Integer> skills;

    public CharClass charClass;
    public Gender gender;
    public Size size=MEDIUM;
    protected Race race;
    public Background suggestedBackground;
    public Background background;
    protected Armor armorWorn=Armor.NONE;
    protected Weapons meleeWeapon=null;
    protected Weapons backupWeapon=null;
    protected Weapons thrownWeapon=null;
    protected Weapons handCrossbowWeapon=null;
    protected int thrownWeaponNumber=0;
    protected Weapons rangedWeapon=null;
    protected int rangedWeaponNumber=0;
    protected int[] spellSlots = new int[]{0,0,0, 0,0,0,0, 0,0,0};

    public String backgroundDescription = "";
    public String ideal;
    public String bond;
    public String flaw;
    public String lifestyle;

    protected Boolean hasShield=false;
    protected Boolean hasFamiliar;

    public Boolean isFinesse=false;
    public Boolean thievesToolsExpert=false;
    public String archetype="";

    public int hp=0;
    public int hd=0;
    public int move=0;
    public int ac=10;
    public int prof=2;
    public int gold=0;

    public int extraLanguages=0;
    public int extraSkills=0;

    public Stats spellAbility;
    public Stats raceSpellAbility;
    protected int cantripsKnown=0;
    protected int spellsKnown=0;
    protected List<String> cantrips = new LinkedList<>();
    protected List<String> wizardsSpellbook = new LinkedList<>();

    public List<String> abil = new LinkedList<String>();
//    public List<String> lang = new LinkedList<String>();
    public List<String> equipment = new LinkedList<String>();
    public List<String> raceSpells = new LinkedList<String>();
    public List<String> tools = new LinkedList<String>();
    public List<String> profList = new LinkedList<String>();
    public List<String> nonProfList = new LinkedList<String>();
    public List<String> personality = new LinkedList<>();
    protected List<Stats> preferredStats = new LinkedList<>();
    protected List<Languages> languages = new LinkedList<>();
    protected List<FightingStyle> fightingStyles = new LinkedList<>();


//    public Character() {
//        race_old = Race_Old.selectRace();
//        charClass = CharClass.selectClass();
//        suggestedBackground = CharClass.getSuggestedBackground(charClass);
//    }

    public Character(Gender gender, Race race, CharClass charClass, Background background, int lvl) {
        languages.add(COMMON);

        stats = new HashMap<Stats, Integer>();
        savingThrows = new HashMap<Stats, Integer>();
        savingThrowProficiencies = new HashMap<Stats, Boolean>();
        for (Stats s : Stats.values()) {
            stats.put(s,0);
            savingThrows.put(s,0);
            savingThrowProficiencies.put(s, false);
        }

        skills = new HashMap<Skills, Integer>();
        for (Skills s : Skills.values()) {
            this.skills.put(s, 0);
        }

        if (gender == null) {
            if (Roll.d(2) == 2) {
                this.gender=MALE;
            } else {
                this.gender=FEMALE;
            }
        } else {
            this.gender=gender;
        }

        if (race == null) {
            this.race = Race.selectRace();
        } else {
            this.race = race;
        }

        if (charClass == null) {
            this.charClass = CharClass.selectClass();
        } else {
            this.charClass = charClass;
        }

        if (background != null) {
            this.background = background;
        }

        if (lvl > 0) {
            this.lvl = lvl;
        }
        else {
            this.lvl = Roll.d(10);
        }
    }


    public static void setProf(Character c) {
        if (c.lvl >= 5) {
            c.prof=3;
        }
        if (c.lvl >= 9) {
            c.prof=4;
        }
    }

    public static void printCharacter(Character c) {

        System.out.println("*=*=*=***  Level " + c.lvl + " " + c.race.getName() + " " +c.charClass.getName()+" (" + c.archetype + ") ");
        System.out.println("HP " + c.hp + " / Prof bonus: " + Util.showPlus(c.prof) + "/ AC: "+c.ac + " armor:" + c.armorWorn.getName());
        if (c.meleeWeapon != null) {

            System.out.println(Weapons.showMeleeWeapon(c));
//            System.out.println(c.meleeWeapon.getName() + " (" +
//                    Util.showPlus(Weapons.calcToHit(c, c.meleeWeapon)) + " to hit) " +
//                    c.meleeWeapon.getDamage() +
//                    Util.showPlusNotZero(Weapons.calcDamageBonus(c, c.meleeWeapon)) + " " +
//                    c.meleeWeapon.getDamageType().getName());
        }
        if (c.backupWeapon != null)
            System.out.println(c.backupWeapon.getName() + " (" +
                    Util.showPlus(Weapons.calcToHit(c, c.backupWeapon)) + " to hit) " +
                    c.backupWeapon.getDamage() +
                    Util.showPlusNotZero(Weapons.calcDamageBonus(c, c.backupWeapon)) + " " +
                    c.backupWeapon.getDamageType().getName());
//            System.out.println("Backup:  " + c.backupWeapon.getName());
        if (c.thrownWeapon != null)
            System.out.println((c.thrownWeaponNumber == 1 ? "" : c.thrownWeaponNumber + " ") +
                    c.thrownWeapon.getName() + (c.thrownWeaponNumber > 1 ? "s" : "") + " (" +
                    Util.showPlus(Weapons.calcToHit(c, c.thrownWeapon)) + " to hit) " +
                    c.thrownWeapon.getDamage() +
                    Util.showPlusNotZero(Weapons.calcDamageBonus(c, c.thrownWeapon)) + " " +
                    c.thrownWeapon.getDamageType().getName() +
                    " " + c.thrownWeapon.getRange());
//            System.out.println("Thrown:  "+ c.thrownWeapon.getName() + " qty: "+ c.thrownWeaponNumber);
        if (c.handCrossbowWeapon != null)
            System.out.println(c.handCrossbowWeapon.getName() + " (" +
                    Util.showPlus(Weapons.calcToHit(c, c.handCrossbowWeapon)) + " to hit) " +
                    c.handCrossbowWeapon.getDamage() +
                    Util.showPlusNotZero(Weapons.calcDamageBonus(c, c.handCrossbowWeapon)) + " " +
                    c.handCrossbowWeapon.getDamageType().getName() +
                    " " + c.handCrossbowWeapon.getRange() +
                    " (40 " + c.handCrossbowWeapon.getAmmoName() + ")");
//            System.out.println("hand crossbow:  "+ c.handCrossbowWeapon.getName());
        if (c.rangedWeapon != null)
            System.out.println(c.rangedWeapon.getName() + " (" +
                    Util.showPlus(Weapons.calcToHit(c, c.rangedWeapon)) + " to hit) " +
                    c.rangedWeapon.getDamage() +
                    Util.showPlusNotZero(Weapons.calcDamageBonus(c, c.rangedWeapon)) + " " +
                    c.rangedWeapon.getDamageType().getName() +
                    " " + c.rangedWeapon.getRange() +
                    " (40 " + c.rangedWeapon.getAmmoName() + ")");
//            System.out.println("Ranged:  "+ c.rangedWeapon.getName() + " qty: "+ c.rangedWeaponNumber);
        System.out.println(c.gender.getName());

        System.out.println(c.background.getName());
        if (c.backgroundDescription.length() > 0) {
            System.out.println(" Description: "+ c.backgroundDescription);
        }

        System.out.println("Personality: "+c.personality + " Flaw: " + c.flaw + " Bond: " + c.bond + " Ideal: "+c.ideal);
//        System.out.println(charClass.move);
//        System.out.println(charClass.charClass);
        System.out.println("STR: "+ c.stats.get(STRENGTH) + " (" +
                        Util.showPlus(Util.abilityModifier(c.stats.get(STRENGTH))) +
                ") DEX: "+ c.stats.get(DEXTERITY)  + " (" +
                Util.showPlus(Util.abilityModifier(c.stats.get(DEXTERITY))) +
                ") CON: "+ c.stats.get(CONSTITUTION)  + " (" +
                Util.showPlus(Util.abilityModifier(c.stats.get(CONSTITUTION))) +
                ") WIS: "+ c.stats.get(WISDOM)  + " (" +
                Util.showPlus(Util.abilityModifier(c.stats.get(WISDOM))) +
                ") INT: "+ c.stats.get(INTELLIGENCE)  + " (" +
                Util.showPlus(Util.abilityModifier(c.stats.get(INTELLIGENCE))) +
                ") CHA: "+ c.stats.get(CHARISMA)+ " (" +
                Util.showPlus(Util.abilityModifier(c.stats.get(CHARISMA))) + ")");
//        System.out.println("STR: "+ c.st + " DEX: "+ c.de + " CON: "+ c.co + " WIS: "+ c.wi + " INT: "+ c.in  + " CHA: "+ c.ch  );
        System.out.println("Saving throws:  STR: "+ Util.showPlus(c.savingThrows.get(STRENGTH)) + " DEX: "+ Util.showPlus(c.savingThrows.get(DEXTERITY)) + " CON: "+Util.showPlus(c.savingThrows.get(CONSTITUTION)) + " WIS: "+ Util.showPlus(c.savingThrows.get(WISDOM)) + " INT: "+ Util.showPlus(c.savingThrows.get(INTELLIGENCE)) + " CHA: "+ Util.showPlus(c.savingThrows.get(CHARISMA)));
        System.out.println("Abilities: " +Util.displayList((c.abil)));
        System.out.println("Languages: "+ Languages.displayLanguages(c.languages));
        System.out.println("Move: "+c.move +" / "+c.size.getName());
//        sk.acro.value='y';
        System.out.println(Skills.printSkills(c));
        if (c.thievesToolsExpert==true) {
            System.out.println("Thieves tools proficiency");
        }
//        Skills.printSkills(c, sk);
        System.out.println("Equipment: "+ Util.displayList(c.equipment));
        if (c.tools.size() > 0) {
            System.out.println("Tool proficiencies: " + Util.displayList(c.tools));
        }
        if (c.spellsKnown > 0 || c.cantripsKnown > 0) {
            System.out.println("spells known: " + c.spellsKnown + " cantrips known:  " + c.cantripsKnown);
        }
        if (c.raceSpells.size() > 0) {
            System.out.println("Race spells: " + Util.displayList(c.raceSpells) + " Race spell ability: "+c.raceSpellAbility);
        }

    }


    public static void calculateHitPoints(Character c) {
        int hp=0;
        //max 1st level hp
        hp += c.hd;
//        imt r=0;
        String log ="";
        double total=0;
        for (int i=2; i <= c.lvl; i++) {
            int r=Roll.d(c.hd);
            hp += r;
            total += r;
            log += "level "+i+" hp roll="+r+ " newhp="+hp+"/";
        }
        if (c.lvl > 1)
            log += "/ average roll = "+ (double) (total/(c.lvl-1))+"/";
        int bonus = Util.abilityModifier(c.stats.get(CONSTITUTION));
        if (bonus > 0) {
            bonus *= c.lvl;
            hp += bonus;
            log += " con bonus "+bonus+"/";
        }
        // dragon sorc bonus
        if(c.race.equals("Hill Dwarf")) {
            hp +=  c.lvl;
            log += "hill dwarf +"+c.lvl;
        }
        c.hp = hp;

        log += "/ hp="+c.hp;
        System.out.println(log);
    }




    public static void addMusic(Character c, int howMany, boolean addEquipment) {
        while (howMany > 0) {
            String m = Util.pickOneString(Const.music);
            if (c.tools.contains(m)) {
                System.out.println("already have this instrument");
            } else {
                howMany--;
                c.tools.add(m);
                if (addEquipment) {
                    c.equipment.add(m);
                }
            }
        }
    }

    public static void addTools(Character c, int howMany, boolean addEquipment, boolean isGuildArtisan) {
        while (howMany > 0) {
            String tool = Util.pickOneString(Const.artisan);
            if (c.tools.contains(tool)) {
                System.out.println("already have this tool");
            } else {
                howMany--;
                c.tools.add(tool+"'s tools");
                if (addEquipment) {
                    c.equipment.add(tool + "'s tools");
                }
                if (isGuildArtisan) {
                    c.backgroundDescription += tool;
                }
            }
        }
    }

    public static void addTools(Character c, int howMany, boolean addEquipment) {
        addTools(c,howMany,addEquipment,false);
    }


}
