package com.listo;

import com.listo.Util.Util;

import static com.listo.Stats.*;

public class Spells {

    //    var familiars = ["Bat","Bat","Cat","Cat","Cat","Crab","Frog (toad)","Hawk","Hawk","Lizard","Octopus","Owl","Owl","Owl","Poisonous Snake","Fish (quipper)","Rat","Rat","Rat","Raven","Raven","Raven","Sea Horse","Spider","Weasel","Weasel"];

    public static void prepareForSpells(Character  c) {

        switch(c.charClass) {
            case BARD:
                c.cantripsKnown=findCantripsKnown(2, c);
                c.spellsKnown=c.lvl+3;
                if (c.lvl == 10) {
                    c.spellsKnown=14;
                }
                getStandardSpellSlots(c);
                break;
            case CLERIC:
                c.cantripsKnown=findCantripsKnown(3, c);
                c.spellsKnown = Util.abilityModifier(c.stats.get(WISDOM)) + c.lvl;
                getStandardSpellSlots(c);
                break;
            case DRUID:
                c.cantripsKnown=findCantripsKnown(2, c);
                c.spellsKnown = Util.abilityModifier(c.stats.get(WISDOM)) + c.lvl;
                getStandardSpellSlots(c);
                break;
            case FIGHTER:
                if (c.archetype == "Eldritch Knight") {
                    if (c.lvl >= 3) {
                        getFighterRogueSpellSlots(c);
                        c.spellsKnown=getFighterRogueSpellsKnown(c);
                        c.cantripsKnown = 2;
                        if (c.lvl >= 10) {
                            c.cantripsKnown=3;
                        }
                    }
                }
                break;
            case PALADIN:
                if (c.lvl >= 2) {
                    //charisma modifier plus c.lvl/2 rounded down
                    c.spellsKnown = Util.abilityModifier(c.stats.get(CHARISMA)) + (c.lvl /2);
                    if (c.spellsKnown < 1) {
                        c.spellsKnown=1;
                    }
                    getRangerPaladinSpellSlots(c);
                }

                break;
            case RANGER:
                if (c.lvl >= 2) {
                    c.spellsKnown = 1 + (int)Math.ceil((double)c.lvl / 2.0);
                    getRangerPaladinSpellSlots(c);
                }

                break;
            case ROGUE:
                if (c.archetype == "Arcane Trickster" && c.lvl >= 3) {
                    getFighterRogueSpellSlots(c);
                    c.spellsKnown=getFighterRogueSpellsKnown(c);
                    c.cantrips.add("Mage hand");
                    c.cantripsKnown = 2;
                    if (c.lvl >= 10) {
                        c.cantripsKnown = 4;
                    }
                }

                break;
            case SORCERER:
                c.cantripsKnown=findCantripsKnown(4, c);
                c.spellsKnown=c.lvl+1;
                getStandardSpellSlots(c);
                break;
            case WARLOCK:
                c.cantripsKnown=findCantripsKnown(2, c);
                c.spellsKnown=c.lvl+1;
                if (c.lvl == 10) {
                    c.spellsKnown=10;
                }

                break;
            case WIZARD:
                c.cantripsKnown=findCantripsKnown(3, c);
                c.spellsKnown = Util.abilityModifier(c.stats.get(INTELLIGENCE)) + c.lvl;
                // spellbook stuff
                // it starts with 6 1st level spells, and shares all level up spells
                getStandardSpellSlots(c);
                break;
        }
    }

    private static int getFighterRogueSpellsKnown(Character c) {
        int answer=3;
        if (c.lvl >= 4)
            answer=4;
        if (c.lvl >= 7)
            answer=5;
        if (c.lvl >= 8)
            answer=6;
        if (c.lvl >= 10)
            answer=7;
        return answer;

    }

    private static int findCantripsKnown (int startNumber, Character c ) {
        int answer=startNumber;
        if (c.lvl >= 4)
            answer=startNumber+1;
        if (c.lvl >= 10)
            answer=startNumber+2;
        return answer;
    }


    private static void getFighterRogueSpellSlots(Character c) {
        switch (c.lvl) {
            case  1: c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            case  2: c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            case  3: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  4: c.spellSlots =  new int[]{3,0,0, 0,0,0,0, 0,0,0}; break;
            case  5: c.spellSlots =  new int[]{3,0,0, 0,0,0,0, 0,0,0}; break;
            case  6: c.spellSlots =  new int[]{3,0,0, 0,0,0,0, 0,0,0}; break;
            case  7: c.spellSlots =  new int[]{4,2,0, 0,0,0,0, 0,0,0}; break;
            case  8: c.spellSlots =  new int[]{4,2,0, 0,0,0,0, 0,0,0}; break;
            case  9: c.spellSlots =  new int[]{4,2,0, 0,0,0,0, 0,0,0}; break;
            case  10: c.spellSlots =  new int[]{4,3,0, 0,0,0,0, 0,0,0}; break;
            case  11: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  12: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  13: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  14: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  15: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  16: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  17: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  18: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  19: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  20: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            }
    }

    private static void getRangerPaladinSpellSlots(Character c) {
        switch (c.lvl) {
            case  1: c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            case  2: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  3: c.spellSlots =  new int[]{3,0,0, 0,0,0,0, 0,0,0}; break;
            case  4: c.spellSlots =  new int[]{3,0,0, 0,0,0,0, 0,0,0}; break;
            case  5: c.spellSlots =  new int[]{4,2,0, 0,0,0,0, 0,0,0}; break;
            case  6: c.spellSlots =  new int[]{4,2,0, 0,0,0,0, 0,0,0}; break;
            case  7: c.spellSlots =  new int[]{4,3,0, 0,0,0,0, 0,0,0}; break;
            case  8: c.spellSlots =  new int[]{4,3,0, 0,0,0,0, 0,0,0}; break;
            case  9: c.spellSlots =  new int[]{4,3,2, 0,0,0,0, 0,0,0}; break;
            case  10: c.spellSlots =  new int[]{4,3,2, 0,0,0,0, 0,0,0}; break;
            case  11: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  12: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  13: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  14: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  15: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  16: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  17: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  18: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  19: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  20: c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
        }
    }

    private static void getStandardSpellSlots(Character c) {
        switch (c.lvl) {
            case  1:c.spellSlots =  new int[]{2,0,0, 0,0,0,0, 0,0,0}; break;
            case  2:c.spellSlots =  new int[]{3,0,0, 0,0,0,0, 0,0,0}; break;
            case  3:c.spellSlots =  new int[]{4,2,0, 0,0,0,0, 0,0,0}; break;
            case  4:c.spellSlots =  new int[]{4,3,0, 0,0,0,0, 0,0,0}; break;
            case  5:c.spellSlots =  new int[]{4,3,2, 0,0,0,0, 0,0,0}; break;
            case  6:c.spellSlots =  new int[]{4,3,3, 0,0,0,0, 0,0,0}; break;
            case  7:c.spellSlots =  new int[]{4,3,3, 1,0,0,0, 0,0,0}; break;
            case  8:c.spellSlots =  new int[]{4,3,3, 2,0,0,0, 0,0,0}; break;
            case  9:c.spellSlots =  new int[]{4,3,3, 3,1,0,0, 0,0,0}; break;
            case  10:c.spellSlots =  new int[]{4,3,3, 3,2,0,0, 0,0,0}; break;
            case  11:c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            case  12:c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            case  13:c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            case  14:c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            case  15:c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            case  16:c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            case  17:c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            case  18:c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            case  19:c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            case  20:c.spellSlots =  new int[]{0,0,0, 0,0,0,0, 0,0,0}; break;
            

        }
    }



    protected static void getSpells(Character c) {
        System.out.println("get spells method");
        switch (c.charClass) {
            case BARBARIAN:
                break;
            case BARD:
                break;
            case CLERIC:
                break;
            case DRUID:
                break;
            case FIGHTER:
                break;
            case MONK:
                break;
            case PALADIN:
                break;
            case RANGER:
                break;
            case ROGUE:
                break;
            case SORCERER:
                break;
            case WARLOCK:
                break;
            case WIZARD:
                break;
        }
    }

}
