package com.listo;

import com.listo.Util.Const;
import com.listo.Util.Util;

import static com.listo.Skills.*;

public enum Background implements Pickable {
    ACOLYTE("Acolyte"),
    CHARLATAN("Charlatan"),
    CRIMINAL("Criminal"),
    ENTERTAINER("Entertainer"),
    FOLK_HERO("Folk Hero"),
    GUILD_ARTISAN("Guild Artisan"),
    HERMIT("Hermit"),
    NOBLE("Noble"),
    OUTLANDER("Outlander"),
    SAGE("Sage"),
    SAILOR("Sailor"),
    SOLDIER("Soldier"),
    URCHIN("Urchin");

    private final String name;
    Background (String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
        	 
    public static void selectBackground(Character c) {
        // don't overwrite existing background
        if (c.background != null ) {
            return;
        }
        // 40% CHANCE of going w/ default bg
        int x=Roll.d(10);
        if (x <= 4) {
            System.out.println("default background!");
            c.background=c.suggestedBackground;
        } else {
            Background[] bgs = new Background[]{ACOLYTE,  CHARLATAN,  CRIMINAL,  ENTERTAINER,  FOLK_HERO,  GUILD_ARTISAN,  HERMIT,  NOBLE,  OUTLANDER,  SAGE,  SAILOR,  SOLDIER,  URCHIN};
            int roll= Roll.d(bgs.length)-1;
            c.background=bgs[roll];
        }
    }

    public static Background getSuggestedBackground(CharClass charClass) {
        switch (charClass) {

            case BARBARIAN:
                return OUTLANDER;

            case BARD:
                return ENTERTAINER;

            case CLERIC:
                return ACOLYTE;

            case DRUID:
                return HERMIT;

            case FIGHTER:
                return SOLDIER;
            case MONK:
                return HERMIT;

            case PALADIN:
                return NOBLE;

            case RANGER:
                return OUTLANDER;

            case ROGUE:
                int x=Roll.d(3);
                if (x==1) return CHARLATAN;
                if (x==2) return URCHIN;
                if (x==3) return CRIMINAL;

            case SORCERER:
                return HERMIT;

            case WARLOCK:
                return CHARLATAN;

            case WIZARD:
                return SAGE;

            default:
                return null;

        }

    }

    public static void backgroundAdjustments(Character c) {

//personality, ideal, bond, flaw

        switch (c.background) {
            case ACOLYTE:
                c.personality=Util.pickMultipleStrings(Const.acolytePersonality, 2);
                c.ideal=Util.pickOneString(Const.acolyteIdeal);
                c.flaw=Util.pickOneString(Const.acolyteFlaw);
                c.bond=Util.pickOneString(Const.acolyteBond);
                c.skills.put(INSIGHT, 1);
                c.skills.put(RELIGION, 1);

                c.extraLanguages += 2;
                c.lifestyle = "";
                Util.addIfNone(c.equipment, "Holy symbol");
                c.equipment.add("Prayer book");
                c.equipment.add("Common clothes");
                c.gold += 15;
                break;
            case CHARLATAN:
                c.personality=Util.pickMultipleStrings(Const.charlatanPersonality, 2);
                c.ideal=Util.pickOneString(Const.charlatanIdeal);
                c.flaw=Util.pickOneString(Const.charlatanFlaw);
                c.bond=Util.pickOneString(Const.charlatanBond);
                c.skills.put(DECEPTION, 1);
                c.skills.put(SLEIGHT_OF_HAND, 1);
                int x=Roll.d(6)-1;
                if (x==0)
                    c.backgroundDescription += " (I cheat at games of chance.)";
                if (x==1)
                    c.backgroundDescription += " (I shave coins or forge documents.)";
                if (x==2)
                    c.backgroundDescription += " (I insinuate myself into people’s lives to prey on their weakness and secure their fortunes.)";
                if (x==3)
                    c.backgroundDescription += " (I put on new identities like clothes.)";
                if (x==4)
                    c.backgroundDescription += " (I run sleight-of-hand cons on street corners.)";
                if (x==5)
                    c.backgroundDescription += " (I convince people that worthless junk is worth their hard-earned money.)";
                c.equipment.add("Fine clothes");
                c.equipment.add("Disguise kit");
                c.equipment.add("Weighted dice");
                c.tools.add("Disguise kit");
                c.tools.add("Forgery kit");
                c.gold += 15;
                break;
            case CRIMINAL:
                c.personality=Util.pickMultipleStrings(Const.criminalPersonality, 2);
                c.ideal=Util.pickOneString(Const.criminalIdeal);
                c.flaw=Util.pickOneString(Const.criminalFlaw);
                c.bond=Util.pickOneString(Const.criminalBond);
                c.skills.put(DECEPTION, 1);
                c.skills.put(STEALTH, 1);

                c.gold+=15;
                c.equipment.add("Crowbar");
                c.equipment.add("Dark common clothes");
                c.tools.add(Util.pickOneString(Const.gaming));
                c.tools.add("Thieves' tools");
                //        myLifestyle = 2;
                x = Roll.d(8)-1;
                if (x==0)
                    c.backgroundDescription += " (Blackmailer)";
                if (x==1)
                    c.backgroundDescription += " (Burglar)";
                if (x==2)
                    c.backgroundDescription += " (Enforcer)";
                if (x==3)
                    c.backgroundDescription += " (Fence)";
                if (x==4)
                    c.backgroundDescription += " (Highway robber)";
                if (x==5)
                    c.backgroundDescription += " (Hired Killer)";
                if (x==6)
                    c.backgroundDescription += " (Pickpocket)";
                if (x==7)
                    c.backgroundDescription += " (Smuggler)";
                break;
            case ENTERTAINER:
                c.personality=Util.pickMultipleStrings(Const.entertainerPersonality, 2);
                c.ideal=Util.pickOneString(Const.entertainerIdeal);
                c.flaw=Util.pickOneString(Const.entertainerFlaw);
                c.bond=Util.pickOneString(Const.entertainerBond);
                c.equipment.add("Costume");
                c.skills.put(ACROBATICS, 1);
                c.skills.put(PERFORMANCE, 1);
//        myLifestyle = 3;
                Character.addMusic(c,1,true);
                c.tools.add("Disguise kit");
                c.gold += 15;
                break;

            case FOLK_HERO:
                c.personality=Util.pickMultipleStrings(Const.folkHeroPersonality, 2);
                c.ideal=Util.pickOneString(Const.folkHeroIdeal);
                c.flaw=Util.pickOneString(Const.folkHeroFlaw);
                c.bond=Util.pickOneString(Const.folkHeroBond);
                c.equipment.add("Common clothes");
                c.tools.add("Land vehicles");
                Character.addTools(c,1,true);
                c.skills.put(ANIMAL_HANDLING, 1);
                c.skills.put(SURVIVAL, 1);

//        myLifestyle = 3;

                c.gold += 10;
                x=Roll.d(10)-1;
                if (x==0)
                    c.backgroundDescription += " (I stood up to a tyrant’s agents.)";
                if (x==1)
                    c.backgroundDescription += " (I saved people during a natural disaster.)";
                if (x==2)
                    c.backgroundDescription += " (I stood alone against a terrible monster.)";
                if (x==3)
                    c.backgroundDescription += " (I stole from a corrupt merchant to help the poor.)";
                 if (x==4)
                    c.backgroundDescription += " (I led a militia to fight off an invading army.)";
                if (x==5)
                    c.backgroundDescription += " (I broke into a tyrant’s castle and stole weapons to arm the people.)";
                if (x==6)
                    c.backgroundDescription += " (I trained the peasantry to use farm implements as weapons against a tyrant’s soldiers.)";
                if (x==7)
                    c.backgroundDescription += " (A lord rescinded an unpopular decree after I led a symbolic act of protest against it.)";
                if (x==8)
                    c.backgroundDescription += " (A celestial, fey, or similar creature gave me a blessing or revealed my secret origin.)";
                if (x==9)
                    c.backgroundDescription += " (Recruited into a lord’s army, I rose to leadership and was commended for my heroism.)";
                break;
            case GUILD_ARTISAN:
                c.personality=Util.pickMultipleStrings(Const.guildArtisanPersonality, 2);
                c.ideal=Util.pickOneString(Const.guildArtisanIdeal);
                c.flaw=Util.pickOneString(Const.guildArtisanFlaw);
                c.bond=Util.pickOneString(Const.guildArtisanBond);
                c.equipment.add("Guild letter");
                c.equipment.add("Traveler's clothes");
                c.skills.put(INSIGHT, 1);
                c.skills.put(PERSUASION, 1);

                Character.addTools(c, 1, true, true);
//        myLifestyle = 4;
                c.extraLanguages += 1;
                c.gold += 15;
                break;
            case HERMIT:
                c.personality=Util.pickMultipleStrings(Const.hermitPersonality, 2);
                c.ideal=Util.pickOneString(Const.hermitIdeal);
                c.flaw=Util.pickOneString(Const.hermitFlaw);
                c.bond=Util.pickOneString(Const.hermitBond);
                c.equipment.add("Record of Hermit studies");
                c.equipment.add("Common clothes");
                c.equipment.add("Herbalism kit");
                c.skills.put(MEDICINE, 1);
                c.skills.put(RELIGION, 1);
                Util.addIfNone(c.tools, "Herbalism kit");
        //        myLifestyle = 2;
                c.gold += 5;
                c.extraLanguages += 1;
                x=Roll.d(8)-1;
                if (x==0)
                    c.backgroundDescription += " (I was searching for spiritual enlightenment.)";
                if (x==1)
                    c.backgroundDescription += " (I was partaking of communal living in accordance with the dictates of a religious order.)";
                if (x==2)
                    c.backgroundDescription += " (I was exiled for a crime I didn’t commit.)";
                if (x==3)
                    c.backgroundDescription += " (I retreated from society after a life-altering event.)";
                if (x==4)
                    c.backgroundDescription += " (I needed a quiet place to work on my art, literature, music, or manifesto.)";
                if (x==5)
                    c.backgroundDescription += " (I needed to commune with nature, far from civilization.)";
                if (x==6)
                    c.backgroundDescription += " (I was the caretaker of an ancient ruin or relic.)";
                if (x==7)
                    c.backgroundDescription += " (I was a pilgrim in search of a person, place, or relic of spiritual significance.)";
                break;
            case NOBLE:
                c.personality=Util.pickMultipleStrings(Const.noblePersonality, 2);
                c.ideal=Util.pickOneString(Const.nobleIdeal);
                c.flaw=Util.pickOneString(Const.nobleFlaw);
                c.bond=Util.pickOneString(Const.nobleBond);
                c.equipment.add("Fine clothes");
                c.equipment.add("Signet ring");
                c.equipment.add("Scroll of pedigree");
                c.skills.put(HISTORY, 1);
                c.skills.put(PERSUASION, 1);

                c.tools.add(Util.pickOneString(Const.gaming));
                c.gold += 25;
                c.extraLanguages += 1;
                break;
            case OUTLANDER:
                c.personality=Util.pickMultipleStrings(Const.outlanderPersonality, 2);
                c.ideal=Util.pickOneString(Const.outlanderIdeal);
                c.flaw=Util.pickOneString(Const.outlanderFlaw);
                c.bond=Util.pickOneString(Const.outlanderBond);
                c.equipment.add("Hunting trap");
                c.equipment.add("Animal trophy");
                c.equipment.add("Traveler's clothes");
                c.skills.put(ATHLETICS, 1);
                c.skills.put(SURVIVAL, 1);

                Character.addTools(c, 1, false);
//        myLifestyle = 2;
                c.gold += 10;
                c.extraLanguages += 1;
                x= Roll.d(10)-1;
                if (x==0)
                    c.backgroundDescription += " (Forester)";
                if (x==1)
                    c.backgroundDescription += " (Trapper)";
                if (x==2)
                    c.backgroundDescription += " (Homesteader)";
                if (x==3)
                    c.backgroundDescription += " (Guide)";
                if (x==4)
                    c.backgroundDescription += " (Exile or outcast)";
                if (x==5)
                    c.backgroundDescription += " (Bounty hunter)";
                if (x==6)
                    c.backgroundDescription += " (Pilgrim)";
                if (x==7)
                    c.backgroundDescription += " (Tribal nomad)";
                if (x==8)
                    c.backgroundDescription += " (Hunter-gatherer)";
                if (x==9)
                    c.backgroundDescription += " (Tribal marauder)";
                break;
            case SAGE:
                c.personality=Util.pickMultipleStrings(Const.sagePersonality, 2);
                c.ideal=Util.pickOneString(Const.sageIdeal);
                c.flaw=Util.pickOneString(Const.sageFlaw);
                c.bond=Util.pickOneString(Const.sageBond);
                c.equipment.add("Black ink");
                c.equipment.add("Quill");
                c.equipment.add("Colleague's letter");
                c.equipment.add("Common clothes");
                c.skills.put(ARCANA, 1);
                c.skills.put(HISTORY, 1);

//        myLifestyle = 3;
                c.extraLanguages += 2;
                c.gold += 10;
                x=Roll.d(8)-1;
                if (x==0)
                    c.backgroundDescription += " (Alchemist)";
                if (x==1)
                    c.backgroundDescription += " (Astronomer)";
                if (x==2)
                    c.backgroundDescription += " (Discredited academic)";
                if (x==3)
                    c.backgroundDescription += " (Librarian)";
                if (x==4)
                    c.backgroundDescription += " (Professor)";
                if (x==5)
                    c.backgroundDescription += " (Researcher)";
                if (x==6)
                    c.backgroundDescription += " (Wizard's apprentice)";
                if (x==7)
                    c.backgroundDescription += " (Scribe)";

                break;
            case SAILOR:
                c.personality=Util.pickMultipleStrings(Const.sailorPersonality, 2);
                c.ideal=Util.pickOneString(Const.sailorIdeal);
                c.flaw=Util.pickOneString(Const.sailorFlaw);
                c.bond=Util.pickOneString(Const.sailorBond);
                c.equipment.add("50 feet silk rope");
                c.equipment.add("Lucky charm");

                c.equipment.add("Trinket");
                c.equipment.add("Common clothes");
                c.skills.put(ATHLETICS, 1);
                c.skills.put(PERCEPTION, 1);

//        myLifestyle = 3;
                c.tools.add("Navigator's tools");
                c.tools.add("Water vehicles");
                c.gold += 10;
                break;
            case SOLDIER:
                c.personality=Util.pickMultipleStrings(Const.soldierPersonality, 2);
                c.ideal=Util.pickOneString(Const.soldierIdeal);
                c.flaw=Util.pickOneString(Const.soldierFlaw);
                c.bond=Util.pickOneString(Const.soldierBond);
                c.equipment.add("Insignia of rank");
                c.equipment.add("Common clothes");
                c.equipment.add("War trophy");
                c.equipment.add("Bone dice");
                c.tools.add("Land vehicles");
                c.skills.put(ATHLETICS, 1);
                c.skills.put(INTIMIDATION, 1);
                c.tools.add(Util.pickOneString(Const.gaming));
//        myLifestyle = 3;

                c.gold += 10;
                x=Roll.d(8)-1;
                if (x==0)
                    c.backgroundDescription += " (Officer)";
                if (x==1)
                    c.backgroundDescription += " (Scout)";
                if (x==2)
                    c.backgroundDescription += " (Infantry)";
                if (x==3)
                    c.backgroundDescription += " (Cavalry)";
                if (x==4)
                    c.backgroundDescription += " (Healer)";
                if (x==5)
                    c.backgroundDescription += " (Quartermaster)";
                if (x==6)
                    c.backgroundDescription += " (Standard bearer)";
                if (x==7)
                    c.backgroundDescription += " (Support staff)";
                break;
            case URCHIN:
                c.personality=Util.pickMultipleStrings(Const.urchinPersonality, 2);
                c.ideal=Util.pickOneString(Const.urchinIdeal);
                c.flaw=Util.pickOneString(Const.urchinFlaw);
                c.bond=Util.pickOneString(Const.urchinBond);
                c.equipment.add("Token");
                c.equipment.add("Common clothes");
                c.tools.add("Disguise kit");
                c.tools.add("Thieves' tools");
                c.tools.add("");
                c.skills.put(SLEIGHT_OF_HAND, 1);
                c.skills.put(STEALTH, 1);

//        myLifestyle = 3;
                c.gold += 10;
                break;
            default:
            System.out.println("no bg error!");
                break;

        }
    }


}
